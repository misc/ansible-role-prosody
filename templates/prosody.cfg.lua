-- Global settings go in this section
Host "*"
	
	-- This is the list of modules Prosody will load on startup.
	-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
	modules_enabled = {
			-- Generally required
				"roster"; -- Allow users to have a roster. Recommended ;)
				"saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
				"tls"; -- Add support for secure TLS on c2s/s2s connections
				"dialback"; -- s2s dialback support
			  	"disco"; -- Service discovery
			
			-- Not essential, but recommended
				"private"; -- Private XML storage (for room bookmarks, etc.)
				"vcard"; -- Allow users to set vCards
			
			-- Nice to have
				"legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
				"version"; -- Replies to server version requests
			  	"uptime"; -- Report how long server has been running
			  	"time"; -- Let others know the time here on this server
			  	"ping"; -- Replies to XMPP pings with pongs

			-- Other specific functionality
				--"register"; -- Allow users to register on this server using a client
				"posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
			  	--"console"; -- telnet to port 5582 (needs console_enabled = true)
				--"bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
				--"httpserver"; -- Serve static files from a directory over HTTP
                "pep";
                "offline";
                "lastactivity";
                "compression";
                "privacy";
			  };
	
    log = "*syslog"
    minimum_log_level = info
    pidfile = "/var/run/prosody/prosody.pid"
    c2s_require_encryption = true

Host "{{ prosody_domain }}"

    enable = true
    ssl = {
		key = "/etc/prosody/certs/{{ prosody_domain }}.key";
		certificate = "/etc/prosody/certs/{{ prosody_domain }}.crt";
        options = "no_sslv2", "no_sslv3", "no_ticket", "no_compression", "cipher_server_preference", "single_dh_use", "single_ecdh_use";  
    }

    admins = { 
        {% for admin in prosody_admins %} 
            "{{ admin }}@{{ prosody_domain }}"; 
        {% endfor %}
    }

-- Set up a MUC (multi-user chat) room server on conference.example.com:
-- Component "chat.jabberfr.org" "muc"
