-- Global settings go in this section
plugin_paths = {
    "{{ modules_path }}",
    "{{ custom_modules_path }}"
}

-- This is the list of modules Prosody will load on startup.
-- It looks for mod_modulename.lua in the plugins folder, so make sure that exists too.
modules_enabled = {
    -- Generally required
    "roster"; -- Allow users to have a roster. Recommended ;)
    "saslauth"; -- Authentication for clients and servers. Recommended if you want to log in.
    "tls"; -- Add support for secure TLS on c2s/s2s connections
    "dialback"; -- s2s dialback support
    "disco"; -- Service discovery

    -- Not essential, but recommended
    "private"; -- Private XML storage (for room bookmarks, etc.)

    "vcard4"; -- Allow users to set vCards
    "vcard_legacy";

    -- Nice to have
    "legacyauth"; -- Legacy authentication. Only used by some old clients and bots.
    "version"; -- Replies to server version requests
    "uptime"; -- Report how long server has been running
    "time"; -- Let others know the time here on this server
    "ping"; -- Replies to XMPP pings with pongs

    -- Other specific functionality
    --"register"; -- Allow users to register on this server using a client
    "posix"; -- POSIX functionality, sends server to background, enables syslog, etc.
    --"console"; -- telnet to port 5582 (needs console_enabled = true)
{% if enable_web %}
    "bosh"; -- Enable BOSH clients, aka "Jabber over HTTP"
    "websocket";
    "httpserver"; -- Serve static files from a directory over HTTP
{% endif %}
    "mam";
    "pep";
    "offline";
    "lastactivity";
    --"compression";
    "blocklist";
    "s2s_blacklist";
    "carbons";
    "csi";
    "external_services";
    "server_contact_info";
    "cloud_notify";
    "smacks";
    "bookmarks";
    "poke_strangers";
};

archive_expires_after = "never"

storage = {
    archive2 = "internal";
    blocklist = "sql";
}

log = "*syslog"
minimum_log_level = info
pidfile = "/var/run/prosody/prosody.pid"
c2s_require_encryption = true

{% if blocked_domains|length > 0 %}
s2s_blacklist = {
{% for d in blocked_domains %}
    "{{ d }}";
{% endfor %}
}
{% endif %}



contact_info = {
{% for i in ['abuse', 'admin', 'feedback', 'sales', 'security', 'support'] %}
    {{ i }} = { "mailto:root@{{ domain }}" };
{% endfor %}
}
{% if turn_server is defined %}
external_services = { {
    type = "stun", transport = "udp",
    host = "{{ turn_server }}", port = 3478
}, {
    type = "turn", transport = "udp",
    host = "{{ turn_server }}", port = 3478,
    secret = "{{ turn_password }}"
} }
{% endif %}


ssl = {
    key = "{{ cert_key }}";
    certificate = "{{ cert_crt }}";
    options = "no_sslv2", "no_sslv3", "no_ticket", "no_compression", "cipher_server_preference", "single_dh_use", "single_ecdh_use";
}

-- for XEP-0368
c2s_direct_tls_ports = { 5223 }

 
Include "/etc/prosody/conf.d/*.cfg.lua"
