VirtualHost "{{ domain }}"

    enabled = true
    admins = {
    {% for admin in admins %}
        "{{ admin }}@{{ domain }}";
    {% endfor %}
    }

    {% if disco|length > 0 %}
    disco_items = {
    {% for d in disco %}
        { "{{ d.jid }}", "{{ d.name }}" };
    {% endfor %}
    }
    {% endif %}


