Ansible module used to manage prosody

# Usage

```
$ cat deploy_prosody.yml
- hosts: prosody
  roles:
  - role: prosody
    admins:
    - misc
    domain: example.org
```

You need to specify the domain, using the `domain` parameter, and
a list of admins (optional).

# Blocking domains

In order to fight spam, the module will also deploy mod_s2s_blacklist.
Blocked domains should be set with the blocked_domains parameter like this:

```
$ cat deploy_prosody.yml
- hosts: prosody
  roles:
  - role: prosody
    admins:
    - misc
    domain: example.org
    blocked_domains:
    - spammer.example.com
```

# Apache httpd mod_md integration

By setting use_mod_md_cert to True, the role will directly link to the
mod_md created certificate, shus saving the hassle of using dns-challenge01 or LE
in another way.
